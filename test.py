#!/usr/bin/python
import csv
import os

result = []
try:
    f = open("testaddress.csv")
except Exception as e:
    result.append(f"Sorry testaddress.csv File not found")
    result.append(
        f"Please Copy and rename testaddress_template.csv file to testaddress.csv and add your host and ip in list"
    )
else:
    with f:
        csv_reader = csv.reader(f, delimiter=",")
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                responce = os.popen("ping " + row[1]).read()
                if "TTL" in responce:
                    # print(f"{n} is up")
                    result.append(f"{row[0]} is up")
                else:
                    # print(f"{n} is down")
                    result.append(f"{row[0]} is down")
                line_count += 1
        # return result
print(result)
