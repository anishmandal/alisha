#!/usr/bin/ python
import pyttsx3
import speech_recognition as sr
import datetime
import wikipedia  # pip install wikipedia
import webbrowser
import os

# import speedtest
import my_speedtest as test

# import help file
import helpfile

# import networktest
import my_networktest as networktest

engine = pyttsx3.init("sapi5")
# print(voices)
# voices = engine.getProperty("voices")

engine.setProperty(
    "voice",
    "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0",
)


def speak(audio):
    # pass
    engine.say(audio)
    engine.runAndWait()


def wishMe():
    hour = int(datetime.datetime.now().hour)
    if hour >= 0 and hour < 12:
        speak("Good Morning")
    elif hour >= 12 and hour < 18:
        speak("Good Afternoon")
    else:
        speak("Good Evening")

    speak("Online and ready Sir")


def takeCommand():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening...")
        r.pause_threshold = 1
        audio = r.listen(source)

    try:
        print("Recognizing...")
        query = r.recognize_google(audio, language="en-in")
        print(f"User said: {query}\n")

    except Exception as e:
        # print(e)
        print("Say that again please...")
        return "None"
    return query


if __name__ == "__main__":
    wishMe()
    # takeCommand()
    while True:
        query = takeCommand().lower()

        # Logic for executing tasks based on query
        if "who are you" in query:
            who = helpfile.whoAreYou()
            speak(who)

        elif "help" in query:
            ahelp = helpfile.helptype()
            for i in ahelp:
                print(i)
                speak(i)
        elif "wikipedia" in query:
            speak("Searching Wikipedia...")
            query = query.replace("wikipedia", "")
            results = wikipedia.summary(query, sentences=2)
            speak("According to Wikipedia")
            print(results)
            speak(results)

        elif "open youtube" in query:
            webbrowser.open("youtube.com")

        elif "google" in query:
            query = query.replace("google", "")
            webbrowser.open(f"https://www.google.com/search?q={query}")

        elif "open stackoverflow" in query:
            webbrowser.open("stackoverflow.com")

        elif "play music" in query:
            music_dir = "C:\\Users\\Anish\\Downloads\\music"
            songs = os.listdir(music_dir)
            print(songs)
            os.startfile(os.path.join(music_dir, songs[0]))

        elif "time" in query:
            strTime = datetime.datetime.now().strftime("%H:%M:%S")
            print(f"Sir, the time is {strTime}")
            speak(f"Sir, the time is {strTime}")

        elif "open code" in query:
            codePath = "C:\\Users\\anish\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe"
            os.startfile(codePath)

        elif "speed" in query:
            print("Running speedtest, please wait...")
            speak("Running speedtest, please wait...")
            result = test.test()
            for i in result:
                print(i)
                speak(i)

        elif "network test" in query:
            print("Running network test, it might take some time, please wait...")
            speak("Running network test, it might take some time, please wait...")
            result = networktest.networktest()
            for i in result:
                print(i)
                speak(i)

        elif "stop" in query:
            speak("Good Bye Sir")
            break
